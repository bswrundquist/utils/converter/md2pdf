FROM node

USER root
RUN apt-get update

RUN wget https://github.com/jgm/pandoc/releases/download/2.10.1/pandoc-2.10.1-1-amd64.deb
RUN dpkg -i pandoc-2.10.1-1-amd64.deb

RUN apt-get install -y texlive-latex-base

RUN apt-get install -y texlive-fonts-recommended texlive-latex-recommended
RUN apt-get install -y texlive-xetex

# WORKDIR /opt/
# COPY . .

# RUN pip install -e .