FQN := bswrundquist-utils-converter-md2pdf
MOUNT_PATH := /opt/md2pdf

build:
	docker build -t ${FQN} .

help: build
	docker run -it --rm ${FQN} --help

refresh:
	docker stop ${FQN} || true
	docker rm ${FQN} || true
	docker rmi ${FQN} || true

smoke-test: build
	docker run -it \
		--rm \
		--env-file=.env \
		-v ${PWD}:${MOUNT_PATH} \
		-w ${MOUNT_PATH} \
		-v /tmp:/tmp \
		--name ${FQN} \
		${FQN} \
		pandoc -f markdown -s --output SRS.pdf Software_Requirements_Specification.md

smoke-test-introspect: build
	docker run -it \
		--rm \
		--env-file .env \
		-v ${PWD}:${MOUNT_PATH} \
		-w ${MOUNT_PATH} \
		-v /tmp:/tmp \
		--name ${FQN} \
		${FQN} \
		bash

clean:
	rm -rf .coverage
	rm -rf .idea
	rm -rf  __pycache__
	rm -rf *.egg-info
	rm -rf .pytest_cache
	rm -rf .mypy_cache