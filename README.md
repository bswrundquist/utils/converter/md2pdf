<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [MD2PDF](#md2pdf)
- [Defining CLI](#defining-cli)
- [Commands](#commands)
  - [Unit Tests](#unit-tests)
  - [Fake data](#fake-data)
  - [Smoke Test](#smoke-test)
  - [Code Styling](#code-styling)
    - [Formating](#formating)
    - [Linting](#linting)
    - [Type Check](#type-check)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

[![pipeline status](https://gitlab.com/bswrundquist/utils/converter/md2pdf/badges/master/pipeline.svg)](https://gitlab.com/bswrundquist/utils/converter/md2pdf/-/commits/master)
[![coverage report](https://gitlab.com/bswrundquist/utils/converter/md2pdf/badges/master/coverage.svg)](https://gitlab.com/bswrundquist/utils/converter/md2pdf/-/commits/master)

# <built-in method title of str object at 0x7f2f411799f0>

# Defining CLI

In the `main.py` file a CLI is created using [Typer](https://github.com/tiangolo/typer).

The CLI arguments are defined in the md2pdf method in `main.py`.

# Setting Up Env

In the `.env` file you can define values to be injected as environment variables in the container.

The one that comes with this repo is empty. To fill it with local credentials a normal convention is to create a soft link 
like below.

```bash
rm .env
ln -s ~/.env .env
```

# Commands

In the *Makefile* there are `make` commands defined to simplify local development.

## Unit Tests

Unit tests allow for quick code to make sure what you are writing is clear in how it is supposed to operate but can 
also calculate the testing `coverage` of the code for one measure of code quality.

Shortcut to run unit tests locally.

```bash
make unit-tests
```
## Fake data

For quick testing there is a `fake.py` script that creates a fake *Parquet* using *Pandas* testing utilities.

To create a `test.parquet` file local to work with run below.

```bash
make fake-data
```

## Smoke Test

This will create an image and run the CLI `md2pdf`. 

Smoke tests goes beyond unit testing by making sure it can run within an already standing infrastructure.

A generic smoke test can be run as follows. 

Look in the Makefile to add commands to the smoke test as you change the CLI. 

```bash
make smoke-test
```

## Code Styling

### Formating

Using the `black` package, the Python files are formatted using PEP8 as a guide.

The `make` shortcut can be used.

```bash
make format
```

### Linting

Linting can be done locally and is also part of the `.gitlab-ci.yml` file and it forces us to pass the tests before other
jobs can be run.

This forces code quality that helps the code be readable.

Shortcut available.

```bash
make lint
```

### Type Check

```bash
make type-check
```

